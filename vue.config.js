module.exports = {
    pluginOptions: {
        electronBuilder: {
          builderOptions: {
	    productName: 'Edgy-Entertainment Launcher',
            win: {
              icon: '/src/assets/favicon.ico'
            }
          }
        }
      }
}