'use strict'

import { app, protocol, BrowserWindow, autoUpdater } from 'electron'
import {
  createProtocol,
  /* installVueDevtools */
} from 'vue-cli-plugin-electron-builder/lib'
const isDevelopment = process.env.NODE_ENV !== 'production'
const feedURL = 'https://storage.googleapis.com/telemetaryproject.appspot.com/Edgy-Entertainment Launcher Setup 0.1.0.exe';
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{scheme: 'app', privileges: { secure: true, standard: true } }])

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({ width: 1280, height: 720, frame:false, webPreferences: {
    nodeIntegration: true
  } })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
    console.log('Checking for Update...');
  }

  win.on('closed', () => {
    win = null
  })
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {

  autoUpdater.setFeedURL(feedURL);
  autoUpdater.checkForUpdates();

  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    // Devtools extensions are broken in Electron 6.0.0 and greater
    // See https://github.com/nklayman/vue-cli-plugin-electron-builder/issues/378 for more info
    // Electron will not launch with Devtools extensions installed on Windows 10 with dark mode
    // If you are not using Windows 10 dark mode, you may uncomment these lines
    // In addition, if the linked issue is closed, you can upgrade electron and uncomment these lines
    // try {
    //   await installVueDevtools()
    // } catch (e) {
    //   console.error('Vue Devtools failed to install:', e.toString())
    // }
    autoUpdater.checkForUpdates();
  }
  console.log('Checking for Update...');
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}


//=====================================================================
// AUTO UPDATE
//=====================================================================

// const sendStatusWindow = (text) => {
//   log.info(text);
//   if(mainWindow) {
//     mainWindow.webContents.send('message', text);
//   }
// };

autoUpdater.on('checking-for-update', ()=>{
  console.log('Checking for Update...');
  //sendStatusWindow('Checking for Update...');
})

autoUpdater.on('update-available', info => {
  console.log('Update Available');
  //sendStatusWindow('Update Available');
})

autoUpdater.on('update-not-available', info => {
  console.log('Update Not Available');
  //sendStatusWindow('Update Not Available');
})

autoUpdater.on('error', err => {
  console.log(`Error in Auto Updater: ${err.toString()}`);
  //sendStatusWindow(`Error in Auto Updater: ${err.toString()}`);
})

autoUpdater.on('update-downloaded', info => {
  console.log(`Update available will install now`);
  //sendStatusWindow(`Update available will install now`);

  //wait and then install

  autoUpdater.quitAndInstall();
})