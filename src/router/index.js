import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import WIP from '../views/WIPView.vue'
import TDD from '../views/TDD.vue'
import GDD from '../views/GDD.vue'
import PATCH from '../views/PATCH.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'


Vue.use(VueRouter)
Vue.use(VueAxios, axios)
  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/wip',
    name: 'WIPVue',
    component: WIP
  },
  {
    path: '/TDD',
    name: 'TDDVue',
    component: TDD
  },
  {
    path: '/GDD',
    name: 'GDDVue',
    component: GDD
  },
  {
    path: '/PATCH',
    name: 'PATCHVue',
    component: PATCH
  }
]

const router = new VueRouter({
  routes
})

export default router
