


const state = {
    gamelocation:""
};

const getters = {
    getGameLocation:(state)=>state.gamelocation
};

const actions = {
    setGameLocation({commit},data){
        console.log(`data = ${data}`);
        commit('setStoreGameLocation',data);
    }
};


const mutations = {
    setStoreGameLocation:(state,location)=>(state.gamelocation = location)
};


export default{
    state,
    getters,
    actions,
    mutations
}