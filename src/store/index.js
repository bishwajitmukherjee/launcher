import Vue from 'vue'
import Vuex from 'vuex'
import filestoragemodule from './modules/filestoragemodule.js'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    filestoragemodule
  }
})
